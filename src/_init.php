<?php

// Initialize services here

use http\MPHttpGet;
use http\MPHttpPost;
use http\MPHttpFiles;
use http\UserAgents;
use services\log\LogService;

$httpget = new MPHttpGet();
$httppost = new MPHttpPost();

$httpfiles = new MPHttpFiles();

$user_agent = UserAgents::get();

//$logs = & LogService::getInstance();

//$conn = & Site_Conn::getInstance();

$view = new Site_TemplateView();

// ... this and that ...
