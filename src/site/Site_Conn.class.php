<?php
use services\db\mysql\MySQLConn;
use services\log\LogService;

	class Site_Conn extends MySQLConn {
		
		protected static $instance;
		
		protected function __construct($host, $user, $pass, $db) {
			parent::__construct($host, $user, $pass, $db);
			
			$this->logs = &LogService::getInstance();
			
			//var_dump($this->logs);
		}
		
		public static function &getInstance() {
			
			if (!isset(self::$instance)) {
			    
				if (RUN_PROFILE_GROUP == "cli") {
					self::$instance = false;
					return self::$instance;
				}
			    
				if (RUN_PROFILE_GROUP == "dev") {
					self::$instance = new Site_Conn('localhost', 'root', 'root', 'db');
					return self::$instance;
				}
				
				if (RUN_PROFILE_GROUP == "test") {
					self::$instance = new Site_Conn('localhost', '', '', '');
					return self::$instance;
				}
				
				if (RUN_PROFILE_GROUP == "online") {
					self::$instance = new Site_Conn('localhost', 'user', 'password', 'db');
					return self::$instance;
				}
				
				trigger_error("running_profile_name [" . RUN_PROFILE_NAME . "] not defined in " . __CLASS__ . ".class", E_USER_ERROR);
			}
			
			return self::$instance;
		}
		
	}
