<?php

use mvc\TemplateView;

	class Site_TemplateView extends TemplateView {
		
		public function __construct() {
			parent::__construct();
			
			$this->template = "template1";
			//$this->template = "multi";
			$this->attribs->set('winTitle', '');
			$this->attribs->set('pageTitle', '');
			$this->attribs->set('pageSubTitle', '');
			$this->attribs->set('body_id', '');
			
			$this->attribs->set('meta_description', '');
			$this->attribs->set('meta_keywords', '');
			
			$this->lang = 'en';
			
			$this->ogp = new Site_OGP();
			
			$this->module_path = '/site';
		}
		
		public function echoWinTitle() {
			$this->echoAttribRaw('winTitle');
			
			if (RUN_PROFILE_NAME != 'online') {
				echo(' (' . RUN_PROFILE_NAME . ')');
				echo(' - (' . $this->lang . ')');
			}
		}
	}
?>
