<?php

use html\OGP;

class Site_OGP extends OGP {

    public function __construct() {
	parent::__construct();

	$this->is_xhtml = false;

	$this->setTitle('');
	$this->setType('product');
	$this->setImage('');
	$this->set('site_name', "");
	$this->set('description', "");

	$this->setBaseURL();
    }

}
