<?php
	
	/**
	 * Layer for the Logs
	 * 
	 */
	class Site_LogService extends LogService {
		
		protected static $instance;
		
		protected function __construct() {
			parent::__construct();
		}
		
		public static function &get() {
			if (isset(self::$instance)) {
				return self::$instance;
			} else {
				
				self::$instance = new BP_LogService();
				self::$instance->codename = "my_app";
				
				if (RUN_PROFILE_NAME == "dev1") {
					self::$instance->dir = '/home/me';
					return self::$instance;
				}
				
				if (RUN_PROFILE_NAME == "dev2") {
					self::$instance->dir = '';
					return self::$instance;
				}
				
				if (RUN_PROFILE_NAME == "test") {
					self::$instance->dir = '';
					return self::$instance;
				}
				
				if (RUN_PROFILE_NAME == "online") {
					self::$instance->dir = '/var/log/tmp';
					return self::$instance;
				}
			}
			
			trigger_error("RUN_PROFILE_GROUP [" . RUN_PROFILE_GROUP . "] or RUN_PROFILE_NAME [" . RUN_PROFILE_NAME . "] not defined in " . __CLASS__ . ".class", E_USER_ERROR);
		}
	}
?>
