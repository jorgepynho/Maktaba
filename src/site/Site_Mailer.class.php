<?php
	/**
	 * PHPMailer dependency
	 */
	class Site_Mailer extends PHPMailerPlus {
		
		public function __construct() {
			parent::__construct();
			
			$this->IsMail();
			
			$this->CharSet = 'UTF-8';
			
			$this->IsHTML(false);
			$this->SetFrom('info@example.com', 'Info');
			$this->AddBCC('info_log@example.com');
		}
	}
?>
