<?php

use \http\EnvProfile;

session_start();

date_default_timezone_set("Europe/Lisbon");

function getToDocRoot($append = '') {
    $counts = substr_count($_SERVER['SCRIPT_NAME'], "/");

    if ($counts < 2)
	return ".";

    $str = str_repeat("/..", ($counts - 1));
    $str = substr($str, 1);

    return $str . $append;
}

if (!defined('TO_DOC_ROOT')) {
    define('TO_DOC_ROOT', getToDocRoot()); // esta constante é global :)
}

//echo("TO_DOC_ROOT = " . TO_DOC_ROOT . "<br />");

function __autoload($class_name) {
    $dirs = array();

    $dirs[] = '_classes';
    $dirs[] = 'site';

    $class_name = str_replace('\\', '/', $class_name);

    $count = 0;
    $file_target = "";
    $file_target_ok = "";
    foreach ($dirs as $d) {
	$file_target = TO_DOC_ROOT . "/" . $d . "/" . $class_name . ".class.php";

	//echo($file_target . "<br />" . PHP_EOL);

	if (is_file($file_target)) {
	    //echo($file_target . "<br />" . PHP_EOL);
	    ++$count;
	    $file_target_ok = $file_target;
	}
    }

    //echo($count . "<br />");

    if ($count > 1) {
	exit("ERROR: Several class definition with the same name [$class_name].");
    } elseif ($count == 1) {
	require_once($file_target_ok);
    }
}

$envp = new EnvProfile();
$envp->setRunProfileGroup();
$envp->setRunProfileName();

require("_init.php");
