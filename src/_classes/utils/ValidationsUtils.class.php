<?php
namespace utils;

	class ValidationsUtils {
		
		/**
		Verifica se só tem caracteres normais: letras (sem acentos), numeros e "_"
		 */
		public static function simpleCharsOnly($word) {
			if (strlen($word) < 1) {
				return false;
			}
			
			$matches = preg_match("/[^\w]/i", $word);
			
			if ($matches === 1) {
				return false;
			}
			
			return true;
		}
		
		/**
		 * Valida se é uma palavra portuguesa (apenas tem letras eventualmente com acentos)
		 */
		public static function isWordPT($word) {
			if (strlen($word) < 1) {
				return false;
			}
			
			$matches = preg_match("/[^a-záàâãäéèẽêëíìîĩïóòõôöúùûũü]/i", $word);
			
			if ($matches === 1) {
				return false;
			}
			
			return true;
		}
		
		/**
		 * Valida se é um nome português válido (apenas tem letras e espaços)
		 */
		public static function isNomePT($nome) {
			if (strlen($nome) < 1) {
				return false;
			}
			
			$matches = preg_match("/[^a-záàâãäéèẽêëíìîĩïóòõôöúùûũü ]/i", $nome);
			
			if ($matches === 1) {
				return false;
			}
			
			return true;
		}		
		
		/**
		 * Valida se � um email v�lido
		 */
		public static function isValidEmail($em) {
			if (strlen($em) < 6) {
				return false;
			}
			
			$email_pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i";
			
			$count = preg_match($email_pattern, $em);
			
			if ($count === 1) {
				return true;
			}
			
			return false;
		}
		
		/**
		 * Valida que a data tem o formato 'AAAA-MM-DD'
		 */
		public static function isValidMySQLDate($date) {
			if (strlen($date) < 10) {
				return false;
			}
			
			$arr_dt = explode("-", $date);
			
			if (count($arr_dt) != 3) {
				return false;
			}
			
			return checkdate($arr_dt[1], $arr_dt[2], $arr_dt[0]);
		}
		
		/**
		 * Verifica se a password � v�lida (case insensitive) (acentos s�o inv�lidos)
		 *
		 * @param $pwd a password
		 * @param $min num minimo de caracteres
		 * @param $max num maximo de caracteres
		 * @param $format
		 *				1 - s� aceita letras
		 *				2 - s� aceita letras e numeros
		 */
		public static function isPasswordOK($pwd, $min = 6, $max = 50, $format = 1) {
		
			//echo(1);
		
			if ($format < 1) {return false;}
			if ($format > 2) {return false;}
			
			//echo(2);
			
			$len = strlen($pwd);
			
			if ($len < 1) {return false;}
			if ($len < $min) {return false;}
			if ($len > $max) {return false;}
			
			//echo(3);
			
			$regexp = "";
			if ($format == 1) {
				$regexp = "/[^a-z]/i";
			}
			if ($format == 2) {
				$regexp = "/[\W]/i";
			}
			
			//echo("regexp : $regexp <br />");
			
			$count = preg_match($regexp, $pwd);
			
			if ($count == 1) {
				return false;
			}
			
			return true;
		}
		
		/**
		 * Valida se o num � um telem�vel portugu�s v�lido
		 */
		public static function tlmPT($num) {
			if (strlen($num) != 9) return false;
			
			if (!is_numeric($num)) return false;
			
			$d = intval(substr($num, 0,1));
			
			if ($d != 9) return false;
			
			return true;
		}
		
		/**
		 * Valida se o num � um telefone v�lido
		 */
		public static function tlfPT($num) {
			if (strlen($num) != 9) return false;
			
			if (!is_numeric($num)) return false;
			
			$d = intval(substr($num, 0,1));
			
			if ($d != 2) return false;
			
			return true;
		}
		
		/**
		 * Valida se o num � um telem�vel, ou telefone v�lido
		 */
		public static function generalTlfPt($num) {
			
			$is_tlf = self::tlfPT($num);
			$is_tlm = self::tlmPT($num);
			
			if ($is_tlf || $is_tlm) return true;
			
			return false;
		}
		
		public static function isNIF_PT($nif) {
			if (strlen($nif) != 9) return false;
			
			if (is_numeric($nif)) return true;
			
			return false;
		}
	}
