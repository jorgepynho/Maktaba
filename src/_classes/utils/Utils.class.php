<?php
namespace utils;

use http\HTTPUtils;
use html\E_HTML;

class Utils {
	
	public static function scriptPath() {
		return realpath(".");
	}
	
	public static function echotn($str, $t = 0, $nl = true) {
		echo(str_repeat("\t", $t));
		echo($str);
		if ($nl) echo(PHP_EOL);
	}
	
	public static function echopre($txt) {
		E_HTML::pre(array(), $txt);
	}
	
	public static function echobr($txt, $xhtml = false) {
	    
		$br = $xhtml ? '<br />' : '<br>';
	    
		echo($txt . $br . PHP_EOL);
	}
	
	public static function echoEnt($txt) {
		echo htmlentities($txt);
	}
	
	public static function isNotPage($pg) {
	    
		return strtoupper(HTTPUtils::getScriptName()) != strtoupper($pg);
	}
	
	/**
	 * @param $size o Tamanho
	 * @param $tipo (A)lfanumérico (N)umérico
	 * TODO
	 */
	public static function randomChars($size, $tipo) {
	    return '';
	}
	
	/**
	 * Devolve uma string com o timestamp seguido de 3 digitos aleatórios.
	 */
	public static function getRandomKey($size) {
		$key = "";
		
		for ($x = 0; $x < $size ; ++$x) {
			// 97
			$key .= chr(rand(97, 122));
		}
		
		return $key;
	}
	
	public static function formatPT($num, $dec = 2) {
		return number_format($num, $dec, ',', '.');
	}
	
	/**
	 * para PT
	 */
	public static function formatInt($num) {
		return number_format($num, 0, ',', '.');
	}
	
	/**
	 * Coloca zeros � esquerda at� completar o comprimento pedido
	 * @param $vl O valor a fazer pad
	 * @param $l O comprimento requerido
	 */
	public static function padZeros($vl, $l) {
		$z = str_repeat("0", $l - strlen($vl));
		
		return $z.$vl;
	}
	
	/**
	 * Convert os slashs "\" para "/"
	 */
	public static function convSlash($str) {
		return str_replace("\\", "/", $str);
	}
	
	/**
	 * Devolve true se f�r um n�mero, e positivo ( > 0 )
	 */
	public static function isPosNum($num) {
		if (!is_numeric($num)) {
			return false;
		} else {
			if ($num > 0) {
				return true;
			}
			
			return false;
		}
	}
	
	/**
	 * Indicamos o ficheiro que pretendemos gravar e esta fun��o verifica se o ficheiro j� existe.
	 * Se sim, retorna um nome alternativo (que neste momento ser� �nico).
	 * @param $fn o nome do ficheiro (path completa)
	 */
	public static function getUniqueName($dir, $fl) {
		$fn = $dir . "/" . $fl;
		
		//echo("checking... $fn<br />");
		
		if (is_file($fn)) {
			$f = pathinfo($fn);
			return getUniqueName($dir, $f['filename'] . "_" . rand(100, 999) . "." . $f['extension']);
		} else {
			return $fl;
		}
	}
	
	public static function getLoremIpsum() {
		$li = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
		return $li;
	}
	
	/**
	 * substitui as letras com acentos e os �
	 */
	public static function retiraAcentos($str) {
		$str = preg_replace("{[�����]}", "a", $str);
		$str = preg_replace("{[����]}", "e", $str);
		$str = preg_replace("{[����]}", "i", $str);
		$str = preg_replace("{[�����]}", "o", $str);
		$str = preg_replace("{[����]}", "u", $str);
		$str = preg_replace("{[�]}", "c", $str);
		$str = preg_replace("{[�]}", "n", $str);
		
		$str = preg_replace("{[�����]}", "A", $str);
		$str = preg_replace("{[����]}", "E", $str);
		$str = preg_replace("{[����]}", "I", $str);
		$str = preg_replace("{[�����]}", "O", $str);
		$str = preg_replace("{[����]}", "U", $str);
		$str = preg_replace("{[�]}", "C", $str);
		$str = preg_replace("{[�]}", "N", $str);
		
		return $str;
	}
	
	public static function sendEmail($from, $to, $assunto, $html) {
		global $logs;
		
		if (strlen($to) < 1) {
			$logs->error("Utils::sendEmail() :: [to] vazio. [from: $from], [assunto = $assunto]");
			return false;
		}
		
		$mail = new PHPMailer();
		$mail->IsHTML(true);
		$mail->CharSet = "ISO-8859-1";
		
		$mail->FromName = $from;
		$mail->From = $from;
		$mail->Sender = $from;
		
		$mail->Subject = $assunto;
		
		$mail->AddAddress($to);
		
		$mail->Body = $html;
		
		$ok = $mail->Send();
		
		if ($ok) {
			$logs->info("Utils::sendEmail() :: OK");
		} else {
			$logs->error("Utils::sendEmail() :: " . $mail->ErrorInfo);
		}
		
		return $ok;
	}
	
	/**
	 * substitui os ' e "
	 */
	public static function replaceQuotes($str, $replace = "") {
		return preg_replace("{[\'\"]}", $replace, $str);
	}
	
	/**
	 * Junta os elementos de um array como se fosse um toString()
	 */
	public static function joinArrayPairs($arr, $delim = '"', $glue = " ") {
		$keys = array_keys($arr);
		
		$str = "";
		
		foreach($keys as $k) {
			if (strlen($str) > 0) {
				$str .= $glue;
			}
			
			$str .= $k . "=" . $delim . $arr[$k] . $delim;
		}
		
		return $str;
	}
	
	/**
	 * devolve tambem um array
	 */
	public static function array2NameValuePair($arr, $eq = ' = ', $delim = '\'') {
		$pairs = array();
		foreach($arr as $k => $v) {
			$pairs[] = "$k$eq$delim$v$delim";
		}
		
		return $pairs;
	}
	
	/**
	 * Extrai texto at� X caracteres
	 * @param $clean Faz sempre o clean de HTML
	 */
	public static function extractSampleText($txt1, $limit = 100, $clean = true) {
		$txt2 = trim($txt1);
		
		$txt2 = strip_tags($txt2);
		$txt2 = preg_replace('{\s+?}', " ", $txt2);
		
		if (strlen($txt2) <= $limit) {
			return $txt2;
		}
		
		$words = split(" ", $txt2);
		
		$txt2 = "";
		foreach($words as $w) {
			$txt2 .= $w . " ";
			
			if (strlen($txt2) >= $limit) {
				$txt2 .= " (...)";
				break;
			}
		}
		
		return $txt2;
	}
	
	/**
	 * devolve o match da regexp e mais algumas infos (dia/hora, IP), separados por #
	 */
	public static function parseLogFile($filename, $regexp) {
		
		$keys = array();
		
		$handle = fopen($filename, "r");
		if ($handle) {
			while (!feof($handle)) {
				$line = fgets($handle, 4096);
				
				$mkey = array();
				$matches = preg_match($regexp, $line, $mkey);
				
				$elements = preg_split('/\\s/', $line);
				
/* 				echo("<!--");
				echo($line . "<br />" . PHP_EOL . PHP_EOL);
				
				echo("-->"); */
				
				if ($matches == 1) {
					$keys[count($keys)] = $elements[0] . " " . $elements[1] . "#" . $mkey[0] . "#" . $elements[10];
				}
			}
		}
		
		fclose($handle);
		
		return $keys;
	}
	
	public static function text_trim($text, $max, $str_end) {
		$words = preg_split('/\s/i', $text);
		
		//print_r($words);
		
		$txt = "";
		foreach($words as $w) {
			$txt .= $w . " ";
			
			if (strlen($txt) >= $max) break;
		}
		
		if (strlen($txt) >= $max) $txt .= $str_end;
		
		return $txt;
	}
	
	public static function generatePassword($size = 12, $use_num = true, $use_alpha_lower = false, $use_alpha_upper = true, $use_special = false) {
		$chars = array();
		
		if ($use_num) {
			for($x = 0; $x <= 9; ++$x) {
				$chars[] = $x;
			}
		}
		
		if ($use_alpha_lower) {
			for($x = 97; $x <= 122; ++$x) {
				$chars[] = chr($x);
			}
		}
		
		if ($use_alpha_upper) {
			for($x = 65; $x <= 90; ++$x) {
				$chars[] = chr($x);
			}
		}
		
		if ($use_special) {
			$chars[] = '#';
			$chars[] = '$';
			$chars[] = '%';
			$chars[] = '&';
			$chars[] = '+';
			$chars[] = '@';
		}
		
		//self::echopre(print_r($chars, true));
		
		$new_pass = "";
		while(strlen($new_pass) < $size) {
			
			rand(10, 20);
			rand(50, 100);
			
			$sel_idx = rand(0, count($chars) - 1);
			
			$new_pass .= $chars[$sel_idx];
		}
		
		return $new_pass;
	}
	
	/**
	 * Creates the file with the givem content. And creates all folders path if necessary.
	 * If the file exists, does nothing
	 */
	public static function createFullFilePath($full_path, $content = '') {
		
		//echo $full_path;
		
		if (is_file($full_path)) {return true;}

	$pi = pathinfo($full_path);
		//print_r($pi);
		
		if (is_dir($pi['dirname'])) {
			$dir_ok = true;
		} else {
			$dir_ok = mkdir($pi['dirname'], 0755, true);
		}
		
		if ($dir_ok) {
			file_put_contents($full_path, $content);
		} else {
			return false;
		}
		
		return is_file($full_path);
	}
	
	public static function getCP($cp4, $cp3 = '', $cpLocal = '') {
		$cp = $cp4;
		
		if ($cp3) {
			$cp .= '-' . $cp3;
		}
		
		$cp .= ' ' . $cpLocal;
		
		return $cp;
	}
	
	public static function echoCP($cp4, $cp3, $cpLocal) {
		echo self::getCP($cp4, $cp3, $cpLocal);
	}
	
	/**
	 * Checks if file starts with a BOM
	 * @param type $f
	 * @return boolean True if starts with BOM, false if not, or is not a file
	 */
	public static function fileHasBOM($f) {
	    if (is_file($f)) {
		$BOM = pack("CCC", 0xef, 0xbb, 0xbf);
		$str = file_get_contents($f);
		
		return strncmp($str, $BOM, 3) == 0;
	    }
	    
	    return false;
	}
}
