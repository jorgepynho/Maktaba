<?php
namespace utils;

class DateTimeUtils {

	/**
	 * Returns now.
	 * @param $type 1 - Y-m-d, 2 - H:i:s
	 *
	 */
	public static function getNow($type = 3) {
		if ($type == 1) {return date("Y-m-d", time());}
		if ($type == 2) {return date("H:i:s", time());}
		if ($type == 3) {return date("Y-m-d H:i:s", time());}
		
		return getNow();
	}
	
	public static function getTomorrow($type = 3) {
		if ($type == 1) {return date("Y-m-d", time() + (60 * 60 * 24));}
		if ($type == 2) {return date("H:i:s", time() + (60 * 60 * 24));}
		if ($type == 3) {return date("Y-m-d H:i:s", time() + (60 * 60 * 24));}
		
		return getTomorrow();
	}
	
	public static function getAfterTomorrow($type = 3) {
		if ($type == 1) {return date("Y-m-d", time() + (60 * 60 * 24 * 2));}
		if ($type == 2) {return date("H:i:s", time() + (60 * 60 * 24 * 2));}
		if ($type == 3) {return date("Y-m-d H:i:s", time() + (60 * 60 * 24 * 2));}
		
		return getAfterTomorrow();
	}
	
	/**
	 * Devolve o timestamp
	 */
	public static function getTimestamp() {
		$dt = date("YmdHis"); // data at� aos segundos
		
		$mt = microtime(true);
		$aux = explode(".", $mt);
		
		return $dt.$aux[1];
	}
	
	public static function meses_long($m) {
		$meses = array("", "Janeiro",  "Fevereiro", "Mar�o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
		
		if (isset($meses[$m])) return $meses[$m];
		
		return '';
	}
	
	public static function meses_short() {
		$meses = array("", "Jan",  "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez");
		
		if (isset($meses[$m])) return $meses[$m];
		
		return '';
	}
	
	public static function dias_semana($n) {
		$dias = array("Domingo", "Segunda", "Ter�a", "Quarta", "Quinta", "Sexta", "S�bado");
		
		if (isset($dias[$n])) return $dias[$n];
		
		return '';
	}
		
	/**
	 * WORK IN PROGRESS
	 */
	public static function secondsToTimeArray($secs) {
		$time = array(
					'anos' => 0,
					'meses' => 0,
					'dias' => 0,
					'horas' => 0,
					'minutos' => 0,
					'segundos' => 0
				);
				
		$time['segundos'] = $secs;
		
		if ($time['segundos'] > 59) {
			$time['minutos'] = intval($time['segundos'] / 60);
			$time['segundos'] = $time['segundos'] % 60;
		} else {
			return $time;
		}
		
		if ($time['minutos'] > 59) {
			$time['horas'] = intval($time['minutos'] / 60);
			$time['minutos'] = $time['minutos'] % 60;
		} else {
			return $time;
		}
		
		if ($time['horas'] > 24) {
			$time['dias'] = intval($time['horas'] / 24);
			$time['horas'] = $time['horas'] % 24;
		} else {
			return $time;
		}
		
		if ($time['dias'] > 1461) {
			$time['dias'] = $time['dias'] % 1461;
			//echo($anos4);
		}
		
		/*
		if ($time['dias'] > 1461) { // 1461 = dias em 4 anos (1 bixesto e 3 communs)
			$time['meses'] = intval($time['horas'] / 24);
			$time['dias'] = $time['dias'] % 24;
		} else {
			return $time;
		}*/
		
		return $time;
	}
	
	public static function secondsToTimeString($secs, $ts = ":") {
		$t = secondsToTimeArray($secs);
		
		$str = "";
		
		if ($t['horas'] < 10) {$str .= "0";}
		$str .= $t['horas'];
		
		$str .= $ts;
		
		if ($t['minutos'] < 10) {$str .= "0";}
		$str .= $t['minutos'];
		
		$str .= $ts;
		
		if ($t['segundos'] < 10) {$str .= "0";}
		$str .= $t['segundos'];
		
		return $str;
	}
	
	public static function outputMonthCalendar($year, $month) {
		
	}
	
	public static function macroDays($days) {
		if ($days < 31) return $days . " dias";
		
		$months = floor($days / 30);
		
		if ($months < 2) {
			return "1 mes";
		} elseif ($months < 12)
			return $months . " meses";
		
		$anos = $months / 12;
		
		if ($anos < 2)
			return '> 1 ano';
		
		return '> ' . $anos . " anos";
	}
}
