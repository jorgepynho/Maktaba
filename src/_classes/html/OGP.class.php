<?php

namespace html;

use http\HTTPUtils;
use html\Tag;
use generic\ArrayList;

	class OGP extends ArrayList {
		
		public $is_xhtml = false;
		
		public function __construct() {
			parent::__construct();
			
			$this->set('title', '');
			$this->set('type', '');
			$this->set('image', 'http://' . HTTPUtils::getServerName() . '/favicon.jpg');
			
			$this->set('site_name', "");
		}
		
		public function setTitle($t) {
			$this->set('title', $t);
		}
		
		public function setType($t) {
			$this->set('type', $t);
		}
		
		public function setTypeProduct() {
			$this->setType('product');
		}
		
		public function setImage($i) {
			$this->set('image', 'http://' . $_SERVER['SERVER_NAME'] . $i);
		}
		
		public function setURL($url) {
			$this->set('url', 'http://' . $_SERVER['SERVER_NAME'] . $url);
		}
		
		public function setBaseURL() {
			$this->setURL('/');
		}
		
		public function setCurrentURL() {
			
			$url = $_SERVER['PHP_SELF'];
			
			$qs = HTTPUtils::getQueryString();
			
			if ($qs) {
			    $url .= '?' . $qs;
			}
			
			$this->setURL($url);
		}
		
		public function output() {
			
			foreach($this->alist as $k => $v) {
			    
				$meta_tag = new Tag('meta');
				$meta_tag->property = 'og:' . $k;
				$meta_tag->content = $v;
			    
				echo $meta_tag;
			}
		}
	}

