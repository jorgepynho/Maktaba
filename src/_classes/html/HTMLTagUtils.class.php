<?php
namespace html;

class HTMLTagUtils {
	

    
	public static function createTag($tag = "", $text = "", $attribs = array(), $cdata = false) {
		$tmp = "";
		$atts = "";
		
		//echo("ooo " . $attribs . " xxxxxxxxxxxxx");
		
		if (count($attribs) > 0) {
			$keys = array_keys($attribs);
			
			for ($k = 0; $k < count($keys) ; ++$k) {
				$atts .= " " . $keys[$k] . "=\"" . $attribs[$keys[$k]] . "\"";
			}
		}
		
		$tmp .= "<$tag$atts>";
		
		if ($cdata) $tmp .= "<![CDATA[";
		
		$tmp .= $text;
		
		if ($cdata) $tmp .= "]]>";
		
		$tmp .= "</$tag>";
		
		return $tmp;
	}
	
	public static function createLink($link, $text, $target = '', $attribs = array()) {
		$attribs['href'] = $link;
		
		if ($target) $attribs['target'] = $target;
		
		return self::createTag("a", $text, $attribs);
	}
	
	public static function createImg($src, $alt = '', $attribs = array()) {
		
		$attribs['src'] = $src;
		$attribs['alt'] = $alt;
		
		$img = self::createTag("img", '', $attribs);
		
		$img = str_replace('</img>', '', $img);
		
		return $img;
	}
	
	public static function createTable($rows = "", $attribs = array()) {
		$table = self::createTag("table", $rows, $attribs);
		
		return $table;
	}
	
	public static function createInputTag($type, $name, $value, $attrs = array()) {
		$attrs['type'] = $type;
		$attrs['name'] = $name;
		$attrs['value'] = $value;
		
		$input = self::createTag("input", "", $attrs, "", false);
		
		return $input;
	}
	
	public static function createInputTextTag($name, $value, $attribs = array()) {
		$input = self::createInputTag("text", $name, $value, $attribs);
		
		return $input;
	}
}
