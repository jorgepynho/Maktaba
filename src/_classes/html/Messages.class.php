<?php
namespace html;

	/**
	 * Manage Messages to output to the user
	 */
	class Messages {
		public $msgs;
		
		/**
		 * Constructor
		 */		
		public function __construct() {
			$this->msgs = array();
		}
		
		/**
		 * Adds a message to the collection (page level or session level)
		 * ****************** to test yet !!
		 */
		public function add($m, $session = false) {
			
			if ($session) {
				$_SESSION['messages'][] = $m;
				return;
			}
			
			$this->msgs[] = $m;
		}
		
		public function addList($list) {
			$this->msgs = array_merge($this->msgs, $list);
		}
		
		public function addGenericMsg($session = false) {
			$this->add("OK", $session);
		}
		
		public function addRandomTextMsg($max = 255, $session = false) {
			$msg = Utils::getLoremIpsum();
			
			$msg = substr($msg, 0, $max);
			
			$this->add($msg, $session);
		}
		
		/**
		 * Clears the messages collection
		 */
		public function clear() {
			$this->msgs = array();
		}
		
		/**
		 * Returns the size of the collection
		 */
		public function size() {
			return count($this->msgs);
		}
		
		/**
		 * Junta ao array das mensagens as que est�o na session
		 */
		protected function appendSessionMsgs() {
			
		}
		
		/**
		 * Outputs the messages as a <ul>
		 */
		public function write($ul_id = "messages_list", $li_class = "") {
			
			if (count($this->msgs) > 0) {
				echo("<ul id=\"$ul_id\">" . PHP_EOL);
				foreach($this->msgs as $mx) {
					echo("<li class=\"$li_class\">" . htmlentities($mx, ENT_QUOTES) . "</li>" . PHP_EOL);
				}
				echo("</ul>" . PHP_EOL);
			}
		}
	}
