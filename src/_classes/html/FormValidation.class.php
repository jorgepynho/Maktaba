<?php
namespace html;

	class FormValidation {
		
		private $fields;
		private $rules;
		private $err_msgs;
		
		public function __construct() {
			$this->fields = array();
			$this->err_msgs = array();
			
			$this->rules = array();
			$this->rules[] = 'required';
			$this->rules[] = 'email';
			$this->rules[] = 'numeric';
			$this->rules[] = 'ignore';
			$this->rules[] = 'maxlength';
			$this->rules[] = 'minlength';
			$this->rules[] = 'exactlength';
			$this->rules[] = 'wordsonly';
			$this->rules[] = 'fieldsMatch';
			$this->rules[] = 'simpleList';
			$this->rules[] = 'captcha';
			$this->rules[] = 'regexp';
		}
		
		public function addRule($field, $rule, $msg, $params = array()) {
			
			if (!in_array($rule, $this->rules)) {
				trigger_error("rule [$rule] not found", E_USER_ERROR);
			}
			
			$this->fields[$field]['rules'][] = $rule;
			$this->fields[$field]['msg'][$rule] = $msg;
			$this->fields[$field]['params'][$rule] = $params;
		}
		
		public function validate() {
			
			//print_r($this->fields);
			
			foreach($this->fields as $field => $info) {
				
				foreach($info['rules'] as $r) {
					if (!$this->$r($field, $info['params'][$r])) {
						$this->err_msgs[] = $info['msg'][$r];
						break;
					}
				}
			}
			
			return $this->err_msgs;
		}
		
		protected function required($f, $params = array()) {
			if (isset($_POST[$f])) {
				return ($_POST[$f] == true ? true : false);
			}
			
			return false;
		}
		
		protected function email($f, $params = array()) {
			if (!isset($_POST[$f])) return true;
			if (strlen($_POST[$f]) < 1) return true;
			
			return MPValidations::isValidEmail($_POST[$f]);
		}
		
		protected function numeric($f, $params = array()) {
			if (!isset($_POST[$f])) return true;
			if (strlen($_POST[$f]) < 1) return true;
			
			return is_numeric($_POST[$f]);
		}
		
		protected function maxlength($f, $params = array()) {
			if (!isset($_POST[$f])) return true;
			if (strlen($_POST[$f]) < 1) return true;
			
			if (isset($params['max'])) {
				return (strlen($_POST[$f]) <= $params['max']);
			} else {
				trigger_error("wrong parameter in FormValidation::maxlength()", E_USER_ERROR);
			}
			
			return false;
		}
		
		protected function minlength($f, $params = array()) {
			if (!isset($_POST[$f])) return true;
			if (strlen($_POST[$f]) < 1) return true;
			
			if (isset($params['min'])) {
				return (strlen($_POST[$f]) >= $params['min']);
			} else {
				trigger_error("wrong parameter in FormValidation::minlength()", E_USER_ERROR);
			}
			
			return false;
		}
		
		protected function exactlength($f, $params = array()) {
			//echo "exactlength";
			
			if (!isset($_POST[$f])) return true;
			if (strlen($_POST[$f]) < 1) return true;
			
			if (isset($params['len'])) {
				return (strlen($_POST[$f]) == $params['len']);
			} else {
				trigger_error("wrong parameter in FormValidation::exactlength()", E_USER_ERROR);
			}
			
			return false;
		}
		
		protected function ignore($f, $params = array()) {
			return true;
		}
		
		protected function wordsonly($f, $params = array()) {
			
			//echo($_POST[$f]);
			
			$dummy = array();
			
			$txt = $_POST[$f];
			$txt = Utils::retiraAcentos($txt);
			
			$num = preg_match_all('/[^a-z\s]/i', $txt, $dummy);
			
			//echo $num;
			
			if ($num > 0) {
				return false;
			}
			
			return true;
		}
		
		protected function fieldsMatch($f, $params = array()) {
			if (!isset($_POST[$f])) return true;
			if (strlen($_POST[$f]) < 1) return true;
			
			if (!isset($params['field2'])) trigger_error("Second field is missing, FormValidation::fieldsMatch()", E_USER_ERROR);
			
			$field2 = $params['field2'];
			
			if ($f == $field2) trigger_error("Comparing same field is not allowed, FormValidation::fieldsMatch()", E_USER_ERROR);
			
			if (!isset($_POST[$field2])) return false;
			
			if ($_POST[$f] != $_POST[$field2]) return false;
			
			return true;
		}
		
		protected function simpleList($f, $params = array()) {
			if (!isset($_POST[$f])) return true;
			if (!$_POST[$f]) return true;
			
			//print_r($params);
			
			if (!isset($params['list'])) trigger_error("List does not exist, FormValidation::simpleList()", E_USER_ERROR);
			
			$list = $params['list'];
			
			if (!is_array($list)) trigger_error("List does not exist, FormValidation::simpleList()", E_USER_ERROR);
			
			if (count($list) < 1) trigger_error("List is empty, FormValidation::simpleList()", E_USER_ERROR);
			
			return in_array($_POST[$f], $list);
		}
		
		protected function captcha($f, $params = array()) {
			if (!isset($_POST[$f])) return false;
			if (!$_POST[$f]) return false;
			
			if (!isset($_SESSION)) return false;
			if (!isset($_SESSION['csc'])) return false; //captcha_session_code
			
			return ($_POST[$f] == $_SESSION['csc']);
		}
		
		protected function regexp($f, $params = array()) {
			if (!isset($_POST[$f])) return true;
			if (!$_POST[$f]) return true;
			
			if (!isset($params['re'])) trigger_error("RegExp does not exist, " . __CLASS__ . "::" . __FUNCTION__ . "()", E_USER_ERROR);
			
			return preg_match($params['re'], $_POST[$f]);
		}
		
		public function __toString() {
			return print_r($this->fields, true);
		}
	}
