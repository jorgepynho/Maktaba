<?php

use text\Template;
use html\HTMLUtils;

namespace html;

	class Tag {
		
		private $tag;
		private $text;
		private $isXHTML;
		
		private $data;
		
		private $tag_html;
		private $tag_self_closed_html;
		private $tag_self_closed_xhtml;
		
		public function __construct($tag, $attribs = array(), $txt = '') {
			$this->tag = $tag;
			$this->data = $attribs;
			$this->text = $txt;
			
			$this->isXHTML = false;
			
			$this->tag_html = '<${tag}${attrs}>${content}</${tag}>';
			$this->tag_self_closed_html = '<${tag}${attrs}>';
			$this->tag_self_closed_xhtml = '<${tag}${attrs} />';
		}
		
		public function selfClosed() {
			$self_closed_tags = array('img', 'input', 'hr', 'br', 'meta', 'link');
			
			return in_array($this->tag, $self_closed_tags);
		}
		
		public function __set($name, $value) {
			$this->data[$name] = $value;
		}
		
		public function __get($name) {
			return array_key_exists($name, $this->data) ? $this->data[$name] : NULL;
		}
		
		public function __isset($name) {
			return isset($this->data[$name]);
		}
		
		public function __unset($name) {
			unset($this->data[$name]);
		}
		
		public function __toString() {
			
		    if (!$this->tag) {
			return '';
		    }
		    
		    $tmpl = '';
		    if ($this->selfClosed()) {
			if ($this->isXHTML) {
			    $tmpl = $this->tag_self_closed_xhtml;
			} else {
			    $tmpl = $this->tag_self_closed_html;
			}
		    } else {
			$tmpl = $this->tag_html;
		    }
		    
		    $tag_attribs = HTMLUtils::arrayToTagAttribs($this->data);
		    
		    if ($tag_attribs) {
			$tag_attribs = ' ' . $tag_attribs;
		    }
		    
		    $template = new Template();
		    $template->setTemplate($tmpl);
		    
		    $attribs['tag'] = $this->tag;
		    $attribs['content'] = $this->text;
		    $attribs['attribs'] = $tag_attribs;
		    
		    return $template->apply($attribs);
		}
	}
