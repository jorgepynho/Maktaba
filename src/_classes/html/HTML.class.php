<?php
namespace html;

	class HTML {
		
		public static function br($r = 1, $nl = true) {
			if ($r < 1) $r = 1;
			
			$strX = '<br>';
			
			if ($nl) {$strX .= PHP_EOL;}
			
			return str_repeat($strX, $r);
		}
		
		public static function a($atts = array(), $txt = '') {
			return new Tag(__FUNCTION__, $atts, $txt);
		}
		
		public static function img($src, $alt, $atts = array()) {
			$atts['src'] = $src;
			$atts['alt'] = $alt;
			
			return new Tag(__FUNCTION__, $atts);
		}
		
		public static function h1($atts = array(), $txt = '') {
			return new Tag(__FUNCTION__, $atts, $txt);
		}
		
		public static function h2($atts = array(), $txt = '') {
			return new Tag(__FUNCTION__, $atts, $txt);
		}
		
		public static function div($atts = array(), $txt = '') {
			return new Tag(__FUNCTION__, $atts, $txt);
		}

		public static function pre($atts = array(), $txt = '') {
			return new Tag(__FUNCTION__, $atts, $txt);
		}
		
		public static function option($atts = array(), $txt = '') {
			return new Tag(__FUNCTION__, $atts, $txt);
		}
		
		public static function span($atts = array(), $txt = '') {
			return new Tag(__FUNCTION__, $atts, $txt);
		}
		
		public static function li($atts = array(), $txt = '') {
			return new Tag(__FUNCTION__, $atts, $txt);
		}
		
		public static function script($atts = array(), $txt = '') {
			return new Tag(__FUNCTION__, $atts, $txt);
		}
	}
