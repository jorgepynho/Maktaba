<?php
    namespace html;
    
	class E_HTML {
		
		public static function br($r = 1, $nl = true) {
			echo html::br($r, $nl);
		}
		
		public static function a($txt, $atts = array()) {
			echo html::a($atts, $txt);
		}
		
		public static function h2($txt, $atts = array()) {
			echo html::h2($atts, $txt);
		}
		
		public static function div($txt, $atts = array()) {
			echo html::div($atts, $txt);
		}
		
		public static function option($txt, $atts = array()) {
			echo html::option($atts, $txt);
		}
		
		public static function span($txt, $atts = array()) {
			echo html::span($atts, $txt);
		}
		
		public static function li($txt, $atts = array()) {
			echo html::li($atts, $txt);
		}
		
		public static function script($txt, $atts = array()) {
			echo html::script($atts, $txt);
		}
	}
