<?php
namespace html;

class HTMLUtils {
	
	/**
	 * devolve selected="selected" se valores iguais
	 */
	public static function selected($val1, $val2) {
		if ($val1 == $val2) {
			echo(" selected=\"selected\"");
		}
	}
	
	public static function selMenuItem($current = "", $target = "", $class = "") {
		
		if (!$class) {
			$class = "current";
		}
		
		if ($current == $target) {
			echo("class=\"$class\"");
		}
	}
	
	public static function selPageMenuItem($target = "", $class = "") {
		
		if (!$class) {
			$class = "current";
		}
		
		$pages = array();
		
		if (is_array($target)) {
			$pages = $target;
		} else {
			$pages[] = $target;
		}
		
		if (in_array($_SERVER['SCRIPT_NAME'], $pages)) {
			echo("class=\"$class\"");
		}
	}
	
	/**
	 * devolve checked="checked" se valores iguais
	 */
	public static function checked($current, $target) {
		if ($current == $target) {
			echo(" checked=\"checked\"");
		}
	}
	
	public static function echoOptions($rs, $def) {
		
		while($d = $rs->fetch_array()) {
			echo("<option value=\"" . $d[0] . "\"");
			if ($d[0] == $def) {
				echo(" selected=\"selected\"");
			}
			echo(">");
			echo($d[1]);
			echo("</option>" . PHP_EOL);
		}
	}
	
	public static function echoOptionsArray($list, $def, $nl = true) {
		
		foreach($list as $k => $v) {
			$atts = array();
			$atts['value'] = $k;
			
			//echo $def . "<br>" . PHP_EOL;
			//echo $v . "<br>" . PHP_EOL;
			
			if ($def == $k) {
				$atts['selected'] = 'selected';
			}
			
			ehtml::option($atts, $v);
			echo PHP_EOL;
		}
	}
	
	public static function echoXmlTag($enc = "ISO-8859-1") {
		self::setXMLContentType();
		echo("<?xml version=\"1.0\" encoding=\"$enc\" ?>" . PHP_EOL);
	}
	
	public static function echoXsltTag($file) {
		echo("<?xml-stylesheet type=\"text/xsl\" href=\"$file\"?>" . PHP_EOL);
	}
	
	public static function echoEmail($email, $domain, $text = "") {
		
		$html = "";
		$html .= "<script type=\"text/javascript\">" . PHP_EOL;
		
		$html .= "<!--" . PHP_EOL;
		$html .= "var email = \"$email\";" . PHP_EOL;
		$html .= "var emailHost = \"$domain\";" . PHP_EOL;
		$html .= "var mailTo = email + \"@\" + emailHost;" . PHP_EOL;
		$html .= "var mailText = mailTo;" . PHP_EOL;
		$html .= "" . PHP_EOL;
		$html .= "document.write(\"<a href=\" + \"mail\" + \"to:\" + mailTo + \">\" + mailText + \"<\/a>\");" . PHP_EOL;
		
		$html .= "//-->" . PHP_EOL;
		$html .= "</script>" . PHP_EOL;
		
		echo $html;
	}
	
	// esta vers�o usa o jQUery
	public static function echoEmail_jQ($email, $domain, $text = "") {
		
		$html = "";
		$html .= "<p id=\"email-box_$email\"></p>" . PHP_EOL;
		$html .= "<script type=\"text/javascript\">" . PHP_EOL;
		
		$html .= "<!--" . PHP_EOL;
		
		$html .= "$(document).ready(function() {";
		
		$html .= "var email = \"$email\";" . PHP_EOL;
		$html .= "var emailHost = \"$domain\";" . PHP_EOL;
		$html .= "var mailTo = email + \"@\" + emailHost;" . PHP_EOL;
		$html .= "var mailText = mailTo;" . PHP_EOL;
		$html .= "" . PHP_EOL;
		//$html .= "document.write(\"<a href=\" + \"mail\" + \"to:\" + mailTo + \">\" + mailText + \"<\/a>\");" . PHP_EOL;
		
		$html .= "var linkHTML = \"<a href=\" + \"mail\" + \"to:\" + mailTo + \">\" + mailText + \"<\/a>\";" . PHP_EOL;
		$html .= "$('#email-box_$email').html(linkHTML);" . PHP_EOL;
		
		$html .= "});";
		
		$html .= "//-->" . PHP_EOL;
		$html .= "</script>" . PHP_EOL;
		
		echo $html;
	}
	
	/**
	 * 
	 * @param type $attribs
	 * @param type $delim
	 * @return string
	 */
	public static function arrayToTagAttribs($attribs, $delim = '"') {
	    if (!$attribs) {return '';}
	    
	    $list = array();
	    foreach($attribs as $k => $v) {
		$list[] = $k . '=' . $delim . $v . $delim;
	    }
	    
	    return implode(' ', $list);
	}
}
