<?php
namespace html;

	/**
	 * Manage Errors Messages to output to the user
	 */
	class Errors extends Messages {
		
		public function addGenericErr() {
			$this->add('Ocorreu um erro. Tente de novo. Se o erro persistir contacte o Administrador.');
		}
		
		/**
		 * Outputs the errors as a <ul>
		 */
		public function write($ul_id = "errors_list", $li_class = "") {
			parent::write($ul_id, $li_class);
		}
	}
