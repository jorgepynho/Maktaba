<?php
namespace mvc;

	class Controller {
		
		public $auto_create_action_files;
		public $action_history = array();
		
		public $default_action = '/home';
		
		public $action = '';
		public $action_file = '';
		
		public $run_def_actions = array();
		
		public $run_actions = true;
		
		public $logs = false;
		
		public function __construct() {
			$this->auto_create_action_files = false;
			
			if (RUN_PROFILE_GROUP == 'dev') {
				$this->auto_create_action_files = true;
			}
			
			$this->logs =& DLLogService::get();
			
			$this->action = $this->getRequestAction();
			
			//echo $this->action;
			
			$this->action_history[] = $this->action;
			
			$this->logs->debug('requested action (' . $_SERVER['REQUEST_METHOD'] . '): ' . $this->action);
			
			$this->setActionFile();
		}
		
		public function getRequestAction() {
			
			if (!isset($_SERVER['PATH_INFO'])) return $this->default_action;
			
			$path_info = $_SERVER['PATH_INFO'];
			
			//echo $path_info;
			
			if (strlen($path_info) < 2) return $this->default_action;
			
			// tem de come�ar por um /
			
			if (substr($path_info, 0, 1) != '/') return $this->default_action;
			
			// as actions n�o podem ter pontos '.' (por seguran�a, para evitar os ..)
			
			if (preg_match('/\./i', $path_info)) return $this->default_action;
			
			// se tiver uma barra no fim, tem de se retirar
						
			if (preg_match('/^.*\/$/i', $path_info)) {
				$path_info = substr($path_info, 0, strlen($path_info)-1);
			}
			
			return $path_info;
		}
		
		public function setUserAction($act) {
			
			$this->action = $act;
			
			$this->action_history[] = $this->action;
			
			$this->logs->debug('user action: ' . $this->action);
			
			return $this->setActionFile();
		}
		
		public function setActionFile() {
			//$act2 = str_replace('.', '/', $act);
			
			$this->action_file = "_mvc/actions" . $this->action . ".act.php";
			
			//echo $af . "<br />" . PHP_EOL;
			
			$this->create_action_files();
			
			return $this->action_file;
		}
		
		public function setDefActionsFiles() {
			global $view;
			
			//echo $this->action_file;
			//echo "<br />" . PHP_EOL;
			
			$dirs = explode('/', $this->action_file);
			//print_r($dirs);
			//echo "<br />" . PHP_EOL;
			
			array_pop($dirs);
			array_shift($dirs);
			array_shift($dirs);
			
			//print_r($dirs);
			//echo "<br />" . PHP_EOL;
			
			$def_actions = array();
			
			$start_path = '_mvc/actions';
			$path = '';
			foreach($dirs as $d) {
				
				$path .= '/' . $d;
				
				$def_path = $path . '/_def.act.php';
				
				$def_Action = new stdClass();
				$def_Action->path = $start_path . $def_path;;
				$def_Action->executed = false;
				
				$exists = false;
				foreach($this->run_def_actions as $df) {
					if ($df->path == $def_Action->path) {
						$exists = true;
						break;
					}
				}
				
				if (!$exists) {
					$this->run_def_actions[] = $def_Action;
					
					if ($this->auto_create_action_files) {
						
						foreach($this->run_def_actions as $f) {
							if (!is_file($f->path)) {
								file_put_contents($f->path, '');
								$view->msgs->add($f->path . ' created');
							}
						}
					}
				}
			}
			
			//print_r($this->run_def_actions);
		}
		
		public function create_action_files() {
			global $view;
			
			//echo('<pre>');
			
			//echo "create_action_files started...";
			
			//echo $af;
			
			if (!$this->auto_create_action_files) return;
			
			//echo "checking [$af]...";
			
			if (is_file($this->action_file)) return;
			
			//echo "creating...";
			
			$pi = pathinfo($this->action_file);
			
			//print_r($pi);
			
			$dirname = $pi['dirname'];
			
			$dir_ok = false;
			
			if (is_dir($dirname)) {
				//echo "dir j� existe";
				$dir_ok = true;
			} else {
				$dir_ok = mkdir($dirname, 0755, true);
			}
			
			if ($dir_ok) {
				$view->msgs->add("file [" . $this->action_file . "] for action [" . $this->action . "] created");
				file_put_contents($this->action_file, ""); 
			}
			
			//echo('</pre>');
		}
		
		protected function log_debug($msg) {
			if ($this->logs) {
				$this->logs->debug($msg);
			}
		}
		
		protected function log_error() {
			if ($this->logs) {
				$this->logs->error($msg);
			}
		}
	}
?>