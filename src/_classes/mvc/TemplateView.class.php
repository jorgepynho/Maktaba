<?php
namespace mvc;

use generic\ArrayList;
use html\Messages;
use html\Errors;
use html\OGP;

	class TemplateView {
		public $default_lang;
		public $lang;
		public $template;
		public $prtLink;
		
		public $attribs;
		
		public $modules;
		public $menus;
		public $sidebars;
		public $lang_files;
		
		public $stylesheets;
		public $js;
		
		public $errs;
		public $msgs;
		
		public $ogp;
		
		public $module_path;
		
		public function __construct() {
			$this->default_lang = "pt";
			$this->lang = "pt";
			
			$this->template = "default";
			$this->prtLink = false;
			
			$this->attribs = new ArrayList();
			
			$this->modules = new ArrayList();
			$this->menus = new ArrayList();
			$this->sidebars = new ArrayList();
			$this->lang_files = array();
			
			$this->stylesheets = new ArrayList();
			$this->js = new ArrayList();
			
			$this->msgs = new Messages();
			$this->errs = new Errors();
			
			$this->ogp = new OGP();
			
			$this->module_path = "";
			
			//var_dump($this->errs);
		}
		
		/**
		 * @param $clear Clear all modules
		 */
		public function addModule($type, $data, $clear = false) {
			$mod = array("type" => $type, "data" => $data);
			
			$this->modules->set($this->modules->size() + 1, $mod, $clear);
		}
		
		/**
		 * @param $clear Clear all modules
		 */
		public function addMenu($file, $clear = false) {
			$this->menus->set($this->menus->size() + 1, $file, $clear);
		}
		
		/**
		 * @param $clear Clear all extras
		 */
		public function addSidebar($type, $data, $clear = false) {
			$xtr = array("type" => $type, "data" => $data);
			
			$this->sidebars->set($this->sidebars->size() + 1, $xtr, $clear);
		}
		
		public function addStyleSheet($url, $templateRelative = true, $clear = false) {
			$css = array("url" => $url, "templateRelative" => $templateRelative);
			
			//print_r($css);
			
			$this->stylesheets->set($this->stylesheets->size(), $css, $clear);
		}
		
		public function addJS($url, $templateRelative = true, $clear = false) {
			$js = array("url" => $url, "templateRelative" => $templateRelative);
			
			$this->js->set($this->js->size(), $js, $clear);
		}
		
		public function addLangFile($url, $clear = false) {
			
			if ($clear) {
				$this->lang_files = array();
			}
			
			$this->lang_files[] = $url;
		}
		
		public function getTemplateRoot() {
			return TO_DOC_ROOT . "/_mvc/templates/" . $this->template;
		}
		
		public function echoTemplateRoot() {
			echo $this->getTemplateRoot();
		}
		
		public function getTemplatePath() {
			return $this->getTemplateRoot() . "/html.tmpl.php";
		}
		
		public function echoAttrib($a, $tag = '') {
			
			$def = "atribute [$a] not defined.";
			if (RUN_PROFILE_NAME == 'online') {
				$def = '';
			}
			
			$this->attribs->write($a, $def, $tag);
		}
		
		public function getModulePath($mod) {
			$path = TO_DOC_ROOT . '/_mvc/modules' . $this->module_path . $mod . '.mod.php';
			
			if (RUN_PROFILE_GROUP == 'dev') {
				//file_put_contents($full_path, "ficheiro [$full_path] criado");
				
				Utils::createFullFilePath($path, "ficheiro [$path] criado");
			}
			
			//echo $path;
			return $path;
		}
		
		/**
		 * chama write_raw
		 */
		public function echoAttribRaw($a) {
			$this->attribs->write_raw($a, '');
		}
		
		public function getLangText($txt, $def = "") {
			if (is_array($txt)) {
				if (isset($txt[$this->lang])) {
					return $txt[$this->lang];
				} else {
					return $def;
				}
			} else {
				return $txt;
			}
		}
		
		public function echoLang($txt) {
			echo($this->getLangText($txt));
		}
		
		public function echoLangText($txt) {
			if (isset($txt[$this->lang])) {
				echo htmlentities($txt[$this->lang]);
			}
		}
		
		public function echoLangPath($link = "") {
			if ($this->default_lang != $this->lang) {
				echo "/" . $this->lang;
			}
			
			echo $link;
		}
		
		public function widget($wgt) {
			return TO_DOC_ROOT . '/_mvc/widgets/' . $wgt . '.wgt.php';
		}
				
		public function __toString() {
			return $this->template;
		}
	}
?>
