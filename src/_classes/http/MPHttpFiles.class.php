<?php
namespace http;

use services\log\LogService;

	
	/**
	 * Handles the parameters passed by POST, plus some utility functions
	 */
	class MPHttpFiles extends MPHttpParams {
		
		public $logs;
		
		public function __construct() {
			parent::__construct($_FILES);
			
			$this->logs =& LogService::getInstance();
		}
		
		public function getFilename($key) {
			if (isset($this->target[$key])) {
				return $this->target[$key]['name'];
			}
			
			return "";
		}
		
		public function has($key) {
			$has = parent::has($key);
			
			if (!$has) {
				return false;
			}
			
			if ($this->target[$key]['error'] != UPLOAD_ERR_OK) {
				return false;
			}
			
			return true;
		}
		
		public function saveAll() {
		}
		
		public function save($key, $dir, $name = "", $replace = false) {
			
			//echo("key = $key" . PHP_EOL);
			//echo("dir = $dir" . PHP_EOL);
			
			//echo(0 . PHP_EOL);
			
			//print_r($this->logs);
			
			if (!isset($this->target[$key])) {
				
				if ($this->logs != false) {
					$this->logs->error(__CLASS__ . "::save() :: no key");
				}
				
				return false;
			}
			
			$ufile = $this->target[$key];
			
			//echo(1 . PHP_EOL);
			
			if ($ufile['error'] == UPLOAD_ERR_NO_FILE) {
				
				if ($this->logs != false) {
					$this->logs->debug(__CLASS__ . "::save() :: no file");
				}
				
				return "";
			}
			
			//echo(2 . PHP_EOL);
			
			if ($ufile['error'] != UPLOAD_ERR_OK) {
				
				if ($this->logs != false) {
					$this->logs->error(__CLASS__ . "::save() :: error " . $ufile['error']);
				}
				
				return false;
			}
			
			//echo(3 . PHP_EOL);
			
			if (strlen($name) > 0) {
				$newFileName = $name . $this->tailExtension($ufile['name']);
			} else {
				$newFileName = $ufile['name'];
				$newFileName = Utils::retiraAcentos($newFileName);
				$newFileName = Utils::replaceQuotes($newFileName);
			}
			
			if (!$replace) {
				$newFileName = self::getUniqueName($dir, $newFileName);
			}
			
			//echo("<br />File upload with key [$key], got name: " . $newFileName);
			
			if ($this->logs != false) {
				$this->logs->debug(__CLASS__ . " - File upload with key [$key], got name: " . $newFileName);
			}
			
			//echo(4 . PHP_EOL);
			
			$imgOK = move_uploaded_file($ufile['tmp_name'], $dir . "/" . $newFileName);
			
			if ($imgOK) {
				return $newFileName;
			} else {
				if ($this->logs != false) {
					$this->logs->error(__CLASS__ . " - erro no upload.");
				}
			}
			
			//echo(5 . PHP_EOL);
			
			return false;
		}
		
		public static function saveOrReplace($file_key, $action_key, $dir, $cur_name) {
			
			$fpath = $dir . '/' . $cur_name;
			$exists = is_file($fpath);
			
			if ($action_key == 'K') {
				return $cur_name;
			} elseif ($action_key == 'D') {
				if ($exists) unlink($fpath);
				return '';
			} elseif ($action_key == 'N') {
				if ($exists) unlink($fpath);
				return $this->save($file_key, $dir);
			}
			
			return $cur_name;
		}
		
		public static function getUniqueName($dir, $fl) {
			global $logs;
			
			$fn = $dir . "/" . $fl;
			
			if (isset($logs)) {
				$logs->debug(__CLASS__ . " - checking... $fn<br />");
			}
			
			if (is_file($fn)) {
				$f = pathinfo($fn);
				return self::getUniqueName($dir, $f['filename'] . "_" . rand(100, 999) . "." . $f['extension']);
			} else {
				return $fl;
			}
		}
		
		private function tailExtension($fn) {
			$parts = explode(".", $fn);
			
			//print_r($parts);
			
			if (count($parts) > 1) {
				return "." . $parts[count($parts)-1];
			}
			
			return "";
		}
	}
