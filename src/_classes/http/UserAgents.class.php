<?php
namespace http;

	class UserAgents {
		
		public $user_agents;
		
		public function __construct() {
			
			$this->user_agents = array();
			
			$this->user_agents['facebook'][] = 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)';
			$this->user_agents['facebook'][] = 'facebookexternalhit/1.0 (+http://www.facebook.com/externalhit_uatext.php)';
			$this->user_agents['facebook'][] = 'facebookplatform/1.0 (+http://developers.facebook.com)';
		}
		
		public function getUserAgent() {
			
			$ua = HTTPUtils::getUserAgent();
			
			foreach($this->user_agents as $k => $v) {
				if (in_array($ua, $v)) {
					return $k;
				}				
			}
			
			return '';
		}
		
		public static function get() {
			$ua = new UserAgents();
			
			return $ua->getUserAgent();
		}
	}