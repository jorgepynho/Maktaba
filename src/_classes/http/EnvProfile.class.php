<?php
namespace http;

class EnvProfile {

	private $hosts_local = array('maktaba', 'localhost', '127.0.0.1');
	private $hosts_test = array('test.example.com');
	private $hosts_online = array('example.com', 'www.example.com');

	private $machine_dev1 = array("");
	private $machine_dev2 = array("");

	private $serverName = '';
	private $computerName = '';

	public function __construct() {
		$this->serverName = filter_input(INPUT_SERVER, 'SERVER_NAME', FILTER_SANITIZE_STRING);
		$this->computerName = getenv('COMPUTERNAME');
	}
	
	public function setRunProfileGroup() {
		
		if (defined('RUN_PROFILE_GROUP')) {
		    return;
		}
	    
		if (isset($_SESSION['rpg'])) {
			define('RUN_PROFILE_GROUP', $_SESSION['rpg']);
			return;
		}

		$prof = "";

		if (in_array($this->serverName, $this->hosts_local)) $prof = 'dev';
		if (in_array($this->serverName, $this->hosts_test)) $prof = 'test';
		if (in_array($this->serverName, $this->hosts_online)) $prof = 'online';

		if ($prof) {
			define('RUN_PROFILE_GROUP', $prof);
			$_SESSION['rpg'] = $prof;
		} else {
			exit('no prof group for [' . $this->serverName . '] - [' . $this->computerName . ']');
		}
	}

	public function setRunProfileName() {

		if (defined('RUN_PROFILE_NAME')) {
		    return;
		}
	    
		if (isset($_SESSION['rpn'])) {
			define('RUN_PROFILE_NAME', $_SESSION['rpn']);
			return;
		}

		$prof = "";

		if (RUN_PROFILE_GROUP == 'dev') {

			if (in_array($this->computerName, $this->machine_dev1)) {
				$prof = "dev1";
			}

			if (in_array($this->computerName, $this->machine_dev2)) {
				$prof = "dev2";
			}
		}

		if (RUN_PROFILE_GROUP == 'test') {$prof = "test";}
		if (RUN_PROFILE_GROUP == 'online') {$prof = "online";}

		if ($prof) {
			define('RUN_PROFILE_NAME', $prof);
			$_SESSION['rpn'] = $prof;
		} else {
			exit('no prof name for [' . $this->serverName . '] - [' . $this->computerName . ']');
		}
	}
}
