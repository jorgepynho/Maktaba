<?php
namespace http;

use utils\Utils;

class HTTPUtils {
	
	protected static $my_server_name;
	protected static $http_request_method;
	protected static $query_string;
	protected static $user_agent;
	protected static $script_name;
	
	public static function getServerName() {
	    
	    if (self::$my_server_name) {return self::$my_server_name;}
	    
	    self::$my_server_name = filter_input(INPUT_SERVER, 'SERVER_NAME', FILTER_SANITIZE_STRING);
	    
	    return self::$my_server_name;
	}
	
	public static function getHttpRequestMethod() {
	    if (self::$http_request_method) {return self::$http_request_method;}
	    
	    self::$http_request_method = filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING);
	    
	    return self::$http_request_method;
	}
	
	public static function getQueryString() {
	    if (self::$query_string) {return self::$query_string;}
	    
	    self::$query_string = filter_input(INPUT_SERVER, 'QUERY_STRING', FILTER_SANITIZE_STRING);
	    
	    return self::$query_string;
	}
	
	public static function getScriptName() {
	    if (self::$script_name) {return self::$script_name;}
	    
	    self::$script_name = filter_input(INPUT_SERVER, 'SCRIPT_NAME', FILTER_SANITIZE_STRING);
	    
	    return self::$script_name;
	}
	
	public static function getUserAgent() {
	    if (self::$user_agent) {return self::$user_agent;}
	    
	    self::$user_agent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_STRING);
	    
	    return self::$user_agent;
	}
	
	/**
	 * returns true if the HTTP Method is POST
	 */
	public static function isPost() {
		return self::getHttpRequestMethod() == 'POST';
	}
	
	/**
	 * returns true if the HTTP Method is GET
	 */
	public static function isGet() {
		return self::getHttpRequestMethod() == 'GET';
	}
	
	public static function setContentType($type) {
		header("Content-Type: $type");
	}
	
	public static function setXMLContentType() {
		self::setContentType("text/xml");
	}
	
	public static function joinPostParams() {
		return Utils::joinArrayPairs($_POST);
	}
	
	/**
	 * Converte uma string para ser enviada num URL
	 */
	public static function convertURL($str, $url = true, $utf8 = false) {
		$str2 = $str;
		
		if ($utf8) {
			$str2 = utf8_encode($str2);
		}
		
		if ($url) {
			$str2 = urlencode($str2);
		}
		
		return $str2;
	}
	
	/**
	 * Prepares a string to be a href (does not URLencode)
	 */
	public static function str_url($str) {
		$str = strtolower($str);
		$str = Utils::retiraAcentos($str);
		
		$str = preg_replace('/\s+/i', '_', $str);
		$str = preg_replace('/\W/i', '', $str);
		$str = preg_replace('/_+/i', '_', $str);
		
		return $str;
	}
	
	public static function getPathInfo() {
	    return filter_input(INPUT_SERVER, 'PATH_INFO', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
	}
	
	/**
	 * Analisa a PATH_INFO e extrai um numero
	 */
	public static function getPathInfoId() {
	    
		$pi = self::getPathInfo();
	    
		if (!$pi) {
		    return 0;
		}

		$parts = explode('/', $pi);
		
		foreach($parts as $p) {
		    if (is_numeric($p)) {
			return intval($p);
		    }
		}
		
		return 0;
	}
	
	public static function exitTo($act = '') {
		header('Location: http://' . self::getServerName() . $act);
		exit;
	}
	
	public static function exitTo301($uri) {
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: $uri");
		exit;
	}
	
	public static function exitTo404() {
		header("HTTP/1.1 404 Not Found");
		exit;
	}
	
	public static function exitTo410() {
		header("HTTP/1.1 410 Gone");
		exit;
	}
}
