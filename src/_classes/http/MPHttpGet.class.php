<?php
namespace http;

	/**
	 * Handles the parameters passed by POST
	 */
	class MPHttpGet extends MPHttpParams {
		
		public function __construct() {
			parent::__construct($_GET);
		}
		
		public function getAction() {
			return parent::getString("a", "");
		}
	}

