<?php
namespace http;

	/**
	 * Utility method to get/set the elements of an array with keys
	 */
	class MPHttpParams {
		
		public $target;
		
		public function __construct(&$t = array()) {
			$this->target = $t;
		}
		
		/**
		 * Return the value of the given parameter, or the given default
		 */		
		public function get($p, $def) {
		    isset($this->target[$p]) ? $this->target[$p] : $def;
		}
		
		/**
		 * This will return as a string
		 */
		public function getString($p, $def = "") {
			return (string) $this->get($p, $def);
		}
		
		/**
		 * This will return the length of the param
		 */
		public function getLength($p) {
			return strlen($this->get($p, ""));
		}
		
		/**
		 * This will return as an int type
		 */
		public function getInt($p, $def = 0) {
			return (int) $this->get($p, $def);
		}
		
		/**
		 * This will return as a float type
		 */
		public function getFloat($p) {
			return (float) $this->get($p, 0.0);
		}
		
		/**
		 * This will return as a string with the params separated by commas
		 */
		public function getCSV($p) {
			$prms = $this->getArray($p);
			
			if (is_array($prms)) {
				return join(",", $prms);
			}
			
			return "";
		}
		
		/**
		 * This will return as an array
		 */
		public function getArray($p) {
			return $this->get($p, array());
		}
		
		public function has($p) {
			return isset($this->target[$p]);
		}
		
		public function __toString() {
			return print_r($this->target, true);
		}
	}
?>
