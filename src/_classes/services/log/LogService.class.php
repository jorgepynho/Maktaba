<?php
namespace services\log;

use utils\DateTimeUtils;

	/**
	 * Layer for the Logs
	 * 
	 */
	class LogService {
		public $codename;
		public $logFile;
		public $format;
		public $dir;
		
		protected static $instance;
		
		public $history = array();
		
		public function __clone() {
			trigger_error('Clone is not allowed.', E_USER_ERROR);
		}
		
		protected function __construct($dir = "") {
			$this->codename = "";
			$this->format = "%d %t %l %m";
			$this->logFile = false;
			$this->dir = $dir;
		}
		
		public static function &getInstance() {
			if (isset(self::$instance)) {
				return self::$instance;
			} else {
				
				self::$instance = new LogService();
				self::$instance->codename = "default";
				
				self::$instance->dir = TO_DOC_ROOT;
				return self::$instance;
			}
		}
		
		/**
		 * Escreve no ficheiro
		 * - %l - Level, DEBUG, ERROR, etc...
		 * - %m - The message to write
		 */
		public function out($level, $msg) {

			//$txt = $this->format; //isto vai ser ignoradao
			
			/*$txt = str_replace("%l", $level, $txt);
			$txt = str_replace("%d", DateTimeUtils::getNow(1), $txt);
			$txt = str_replace("%t", DateTimeUtils::getNow(2), $txt);
			$txt = str_replace("%m", $msg, $txt);*/
						
			$txt = '';
			$txt .= DateTimeUtils::getNow(1);
			$txt .= ' ';
			$txt .= DateTimeUtils::getNow(2);
			$txt .= ' ' . $level;
			$txt .= ' ' . $msg;
			//$txt .= PHP_EOL;
			
			$history[] = $txt;
			
			if (!$this->dir) {
				error_log($this->codename . " - " . $txt);
				return;
			}
			
			$fx = $this->codename . "." . DateTimeUtils::getNow(1) . ".log";
			
			//error_log($txt . PHP_EOL, 3, $this->dir . "/$fx");
			
			if (!$this->logFile) {
				$this->logFile = @fopen($this->dir . "/$fx", "a");
			}
			
			if (!$this->logFile) return;
			
			fwrite($this->logFile, $txt . PHP_EOL);
		}
		
		public function sql($msg) {
			$this->out("SQL", $msg);
		}
		
		public function debug($msg) {
			$this->out("DEBUG", $msg);
		}
		
		public function info($msg) {
			$this->out("INFO", $msg);
		}
		
		public function warn($msg) {
			$this->out("WARN", $msg);
		}
		
		public function error($msg) {
			$this->out("ERROR", $msg);
		}
		
		public function closeFile() {
			@fclose($this->logFile);
		}
	}
