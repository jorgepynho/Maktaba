<?php
namespace services\mail;

	class PHPMailerPlus extends PHPMailer {
		
		protected $logs;
		
		public function __construct() {
			parent::__construct();
			
			//$this->logs = &LogService::getInstance();
		}
		
		public function setBodyFromTemplate($tmpl, $info = array()) {
			$objTmpl = new Template();
			$objTmpl->loadTemplate($tmpl);
			$objTmpl->apply($info);
			
			$this->Body = $objTmpl->getTemplateResult();
			
			//echo $this->Body;
		}
	}
?>
