<?php
namespace services\db\mysql;

use services\log\LogService;

	/**
	 * Layer for the DB (MySQLi extension)
	 * 
	 */
	class MySQLConn extends \MySQLi {
		public $host; //host
		public $username; //username
		public $database; //database
		
		public $r; //result
		public $ok; // connection OK
		public $logs; // objecto para fazer o log
		
		private $history;
		
		public function __clone() {
			trigger_error('Clone is not allowed.', E_USER_ERROR);
		}
		
		protected function __construct($host, $user, $pass, $db) {
			parent::__construct($host, $user, $pass, $db);
			
			$this->host = $host;
			$this->username = $user;
			$this->database = $db;
			
			$this->r = false;
			
			$this->ok = false;
			
			$this->logs = &LogService::getInstance();
			
			//echo();
			
			$this->history = array();
			
			if (mysqli_connect_errno() == 0) {
				$this->ok = true;
			}
		}
		
		public function outputStatus() {
			if ($this->ok) {
				echo("Conn OK");
				return;
			}
			
			echo("NO Conn");
		}
		
		public function showVariables($like = '%') {
			$sql = "SHOW VARIABLES LIKE '$like'";
			$vars = $this->getData($sql);
			echo "<pre>";
			while($v = $vars->fetch_array()) {
				echo $v[0];
				echo ": ";
				echo $v[1];
				echo PHP_EOL;
			}
			echo "</pre>" . PHP_EOL . PHP_EOL;
		}
		
		private function addSQLHistory($sql, $err = '', $descr = '') {
			global $running_profile_name;
			
			//echo("addHistory($sql)<br />");
			
			$sql_html = $sql;
			
			if (strlen($err) > 0) {
				$sql_html .= " <span class=\"red\">($err)</span>";
			}
			
			if (strlen($descr) > 0) {
				$sql_html .= " <span class=\"green\">[$descr]</span>";
			}
			
			if (strlen($err) > 0) {
				error_log("SQLERR: [$sql] [$descr] [$err]");
				if (RUN_PROFILE_NAME == 'dev1') {
					echo($sql_html . "<br />" . PHP_EOL);
				}
			}
			
			$this->history[] = $sql_html;
		}
		
		public function echoSQLHistory($block_tag = 'ul', $line_tag = 'li') {
			$html = "";
			
			$highlights_blue = array('DELETE ', 'SELECT DISTINCT ', 'SELECT ', 'UPDATE ', ' SET ', ' FROM ', ' WHERE ', 'INSERT INTO ', ' VALUES ', ' INNER JOIN ', ' LEFT JOIN ', ' LIMIT ', ' ORDER BY ', ' USING', ' NOT NULL', ' AND ', ' OR ', ' ON ');
			//$highlights_red = array(' CONCAT(');
			
			foreach($this->history as $sql) {
				
				$sql2 = $sql;
				foreach($highlights_blue as $hl) {
					$sql2 = str_ireplace($hl, '<span class="blue">' . $hl . '</span>', $sql2);
				}
				
				$html .= HTMLTagUtils::createTag($line_tag, $sql2);
				$html .= PHP_EOL;
			}
			
			$attribs = array("id" => "sql-history-box");
			$attribs = array("style" => "text-align: left;");
			
			$html = HTMLTagUtils::createTag($block_tag, $html, $attribs);
			$html .= PHP_EOL;
			
			echo($html);
		}
		
		public function echoSQL($sql, $nl = true) {
			$sql_keywords = array('DELETE ', 'SELECT DISTINCT ', 'SELECT ', 'UPDATE ', ' SET ', ' FROM ', ' WHERE ', 'INSERT INTO ', ' VALUES ', ' INNER JOIN ', ' LEFT JOIN ', ' LIMIT ', ' ORDER BY ', ' USING', ' NOT NULL', ' AND ', ' OR ', ' ON ');
			$sql_errs = array('Duplicate entry');
			
			$sql_html = $sql;
			foreach($sql_keywords as $hl) {
				$sql_html = str_ireplace($hl, '<span style="color:#0000FF;font-weight:bold;">' . $hl . '</span>', $sql_html);
			}
			
			foreach($sql_errs as $hl) {
				$sql_html = str_ireplace($hl, '<span style="color:#00FF00;font-weight:bold;">' . $hl . '</span>', $sql_html);
			}
			
			echo $sql_html;
			if ($nl) echo "<br />" . PHP_EOL;
		}
		
		public function closeConn() {
			if (!$this->ok) {
				$this->log_debug("closeConn() :: no active connection.");
				return false;
			}
			
			parent::close();
			$this->ok = false;
			
			$this->log_debug("closeConn() :: connection closed.");
		}
		
		public function close() {
			$this->log_debug("close()");
			$this->closeConn();
		}
		
		public function getData($sql, $descr = '') {
			if (!$this->ok) {
				$this->log_warn("getData() :: no active connection for command [$sql].");
				return false;
			}
			
			if ((!isset($sql)) || (!$sql)) {
				$this->log_error("execute() :: comando vazio");
				return false;
			}
			
			$this->log_sql("getData() :: $sql");
			
			//echo($sql);
			
			$this->select_db($this->database);
			$this->r = $this->query($sql);
			
			$this->addSQLHistory($sql, $this->error, $descr);
			
			if ($this->errno > 0) {
				$this->log_error($this->error);
			}
			
			return $this->r;
		}
		
		public function getLastData() {
			return $this->r;
		}
		
		public function execute($sql, $descr = '') {
			if (!$this->ok) {
				$this->log_error("execute() :: no active connection for command [$sql]");
				return false;
			}
			
			if ((!isset($sql)) || (!$sql)) {
				$this->log_error("execute() :: comando vazio");
				return false;
			}
			
			$this->log_sql("execute() :: $sql");
			
			//$this->insert_id = 0;
			$this->select_db($this->database);
			
			$this->query($sql);
			
			$this->addSQLHistory($sql, $this->error, $descr);
			
			if ($this->errno > 0) {
				$this->log_error('(' . $this->errno . ') ' . $this->error . " [$sql]");
				return -1;
			} else {
				if ($this->insert_id > 0) {
					$this->log_debug("new Id = " . $this->insert_id);
					return $this->insert_id;
				}
				
				$this->log_debug("rows = " . $this->affected_rows);
				
				return $this->affected_rows;
			}
		}
		
		public function isDuplicateError() {
			$dup_errs = array(1062, 1586);
			return in_array($this->errno, $dup_errs);
		}
		
		public function getDuplicateErrorKey() {
			if (stripos($this->error, 'Duplicate entry') >= 0) {
				
				$matches = array();
				
				//echo $this->error . "<br>" . PHP_EOL;
				
				preg_match_all('/\'.*?\'/i', $this->error, $matches);
				
				if (count($matches) > 0) {
					
					$match1 = $matches[0];
					
					if (count($match1) > 0)
						return str_replace('\'', '', $match1[count($match1)-1]);
				}
			}
			
			return '';
		}
		
		public function sqlFields($info) {
			
			$info = $this->escape_array($info);
			
			$sql_1 = Utils::array2NameValuePair($info);
			
			$sql = implode(', ', $sql_1);
			
			return $sql;
		}
		
		public function sqlWHERE($where) {
			$sql_1 = Utils::array2NameValuePair($where);
			
			$sql = implode(' AND ', $sql_1);
			
			if ($sql) $sql = 'WHERE ' . $sql;
			
			return $sql;
		}
		
		/**
		 * recebe um array
		 */
		public function insert($tb, $info) {
			if (!is_array($info)) {
				$this->log_error('insert() : \$info n�o � um array');
				return false;
			}
			
			if (count($info) < 1) {
				$sql = "INSERT INTO $tb () VALUES ()";
				return $this->execute($sql);
			}
			
			$sql = "INSERT INTO $tb SET ";
			$sql .= $this->sqlFields($info);
			
			//echo $sql . "<br>" . PHP_EOL;
			
			return $this->execute($sql);
		}
		
		/**
		 * recebe um array
		 */
		public function update($tb, $info, $where) {
			if (!is_array($info)) {
				$this->log_error('update() : \$info n�o � um array');
				return false;
			}
			
			if (count($info) < 1) {
				$this->log_error('update() : sem campos');
				return false;
			}
			
			if (!is_array($where)) {
				$this->log_error('update() : \$where n�o � um array');
				return false;
			}
			
			if (!is_array($where)) {
				$this->log_error('update() : \$where sem campos');
				return false;
			}
			
			$sql = "UPDATE $tb SET ";
			
			$sql .= $this->sqlFields($info);
			$sql .= ' ' . $this->sqlWHERE($where);
			
			return $this->execute($sql);
		}
		
		public function delete($tb, $where, $limit = 1) {
			$sql = "DELETE FROM $tb ";
			$sql .= $this->sqlWHERE($where);
			
			if ($limit) {
				$sql .= " LIMIT $limit";
			}
			
			return $this->execute($sql);
		}
		
		/**
		 * Returns the first field of the first line
		 */
		public function getValue($sql, $ret = 0, $descr = '') {
			$rs = $this->getData($sql, $descr);
			
			if ($rs == false) {return false;}
			
			if ($row = $rs->fetch_array()) {
				return $row[0];
			} else {
				return $ret;
			}
			
			return false;
		}
		
		/**
		 * Returns the first line as an object
		 */
		public function getObject($sql, $descr = '') {
			$rs = $this->getData($sql, $descr);
			
			if ($rs == false) {return false;}
			
			return $rs->fetch_object();
		}
		
		/**
		 * Returns the first line as an object
		 */
		public function getClassObject($sql, $class, $descr = '') {
			$rs = $this->getData($sql, $descr);
			
			if ($rs == false) {return false;}
			
			return $rs->fetch_object($class);
		}
		
		/**
		 * devolve tudo como um array
		 */
		public function getArray($sql, $descr = '') {
			$rs = $this->getData($sql, $descr);
			
			if ($rs == false) {return false;}
			
			$arr = array();
			
			while($row = $rs->fetch_array()) {
				$arr[] = $row;
			}
			
			return $arr;
		}
		
		/**
		 * devolve o primeiro campo como um CSV
		 */
		public function getCSV($sql, $glue = ',', $descr = '') {
			$rs = $this->getData($sql, $descr);
			
			//echo("glue = $glue<br />");
			
			if ($rs == false) {return false;}
			
			$csv = '';
			
			$first = true;
			while($row = $rs->fetch_array()) {
				
				if (!$first) {
					$csv .= $glue;
				}
				
				$csv .= $row[0];
				$first = false;
			}
			
			$rs->free();
			
			return $csv;
		}
		
		/**
		 * Devolve um array de objectos (uma Row � um Object)
		 */
		public function getObjectsArray($sql, $descr = '') {
			$rs = $this->getData($sql, $descr);
			
			$lista = array();
			while($obj = $rs->fetch_object()) {
				$lista[] = $obj;
			}
			
			return $lista;
		}
		
		public function isTable($tb) {
			$sql = "SELECT COUNT(TABLE_NAME) AS num FROM information_schema.TABLES WHERE TABLE_SCHEMA = '" . $this->database . "' AND TABLE_NAME = '$tb' ORDER BY TABLE_NAME";
			$num = $this->getValue($sql);
			
			if ($num > 0) return true;
			
			return false;
		}
		
		public function getTables() {
			$sql = "SELECT * FROM information_schema.TABLES WHERE TABLE_SCHEMA = '" . $this->database . "' ORDER BY TABLE_NAME";
			return $this->getData($sql);
		}
		
		public function getTableFields($tb) {
			//$sql = "SELECT * FROM information_schema.columns WHERE TABLE_NAME = '$tb'";
			$sql = "SELECT * FROM information_schema.columns WHERE TABLE_SCHEMA = '" . $this->database . "' AND TABLE_NAME = '$tb' ORDER BY ordinal_position";
			return $this->getData($sql);
		}
		
		/**
		 * Devolve um array com os nomes dos campos da tabela
		 */
		public function getTableFieldsNames($tb) {
			$sql = "SELECT COLUMN_NAME FROM information_schema.columns WHERE TABLE_SCHEMA = '" . $this->database . "' AND TABLE_NAME = '$tb' ORDER BY ordinal_position";
			$flds = $this->getData($sql);
			$list = array();
			while($f = $flds->fetch_array()) {
				$list[] = $f[0];
			}
			
			return $list;
		}
		
		public function hasError() {
			if ($this->errno > 0) {return true;}
			
			return false;
		}
		
		public function getLastError() {
			return "(" . $this->errno . ") " . $this->error;
		}
		
		/**
		 * Considera-se que um comando atrav�s de "execute" foi bem sucedido quando
		 * n�o h� erros e "affected_rows" � > 0
		 */
		public function executeSuccess() {
			if (($this->errno == 0) && ($this->affected_rows > 0)) return true;
			
			return false;
		}
		
		public function escape($str) {
			return $this->real_escape_string($str);
		}
		
		public function escape_array($info) {
			
			if (!is_array($info)) return $info;
			
			//array_walk($info, array($this, 'escape'));
			
			$keys = array_keys($info);
			
			foreach($keys as $k) {
				$info[$k] = $this->escape($info[$k]);
			}
			
			return $info;
		}
		
		/**
		 * Apaga todos os dados e coloca o AUTO_INCREMENT a 1 (pr�ximo registo)
		 */
		public function resetTable($tb) {
			$this->execute("DELETE FROM $tb");
			$this->execute("ALTER TABLE $tb AUTO_INCREMENT = 1");
		}
		
		public function resetTables($list) {
			
			foreach($list as $t) {
				$this->resetTable($t);
			}
		}
		
		public function free() {
			$this->r->close();
		}
		
		public function html_table($sql, $attribs = array()) {
			
			$table_border = isset($atribs['table_border']) ? $atribs['table_border'] : "1";
			$table_cellpadding = isset($atribs['table_cellpadding']) ? $atribs['table_cellpadding'] : "2";
			$table_cellspacing = isset($atribs['table_cellspacing']) ? $atribs['table_cellspacing'] : "0";
			
			$rs = $this->getData($sql);
			
			$cols_count = $rs->field_count;
			
			//HTMLUtils::createTag
			
			echo("<table border='$table_border' cellpadding='$table_cellpadding' cellspacing='$table_cellspacing'>");
			
			if (isset($attribs['cols_names'])) {
				
				$cols = $attribs['cols_names'];
				
				foreach($cols as $c) {
					echo("<th>$c</th>" . PHP_EOL);
				}				
			} else {
				$cols = $rs->fetch_fields();
				
				foreach($cols as $c) {
					echo("<th>" .  $c->name . "</th>" . PHP_EOL);
				}
			}
			
			while($row = $rs->fetch_array()) {
				echo("<tr>" . PHP_EOL);
				
				for ($c = 0; $c < $cols_count; ++$c) {
					echo("<td>" .  $row[$c] . "</td>" . PHP_EOL);
				}
				
				echo("</tr>" . PHP_EOL);
			}
			echo("</table>" . PHP_EOL);
		}
		
		private function log_sql($msg) {
			if ($this->logs != false) {
				$this->logs->sql('[' . __CLASS__ . '] ' .  $msg);
			}
		}
		
		private function log_debug($msg) {
			if ($this->logs != false) {
				$this->logs->debug('[' . __CLASS__ . '] ' .  $msg);
			}
		}
		
		private function log_error($msg) {
			if ($this->logs != false) {
				$this->logs->error('[' . __CLASS__ . '] ' .  $msg);
			}
		}
		
		private function log_warn($msg) {
			if ($this->logs != false) {
				$this->logs->warn('[' . __CLASS__ . '] ' .  $msg);
			}
		}
		
		public function __toString() {
			return __CLASS__ . " [server = " . $this->host . "], [username = " . $this->username . "], [database = " . $this->database . "]";
		}
	}
?>
