<?php

namespace generic;

/**
 * Works with an array to do various output things
 */
class ArrayList {
	protected $alist;
	
	public function __construct() {
		$this->alist = array();
	}
	
	public function has($key) {
	    return isset($this->alist[$key]);
	}
	
	public function get($key, $def = "") {
	    return $this->has($key) ? $this->alist[$key] : $def;
	}
	
	public function getArray() {
	    return $this->alist;
	}
	
	/**
	 * Output (echo) the value on the given key
	 * @param type $key
	 * @param type $def
	 * @param type $tag
	 */
	public function write($key, $def = '', $tag = '') {
		if ($this->has($key)) {
			$val = $this->alist[$key];
		} else {
			$val = $def;
		}
		
		if ($val && $tag) {
			echo HTMLTagUtils::createTag($tag, $val);
		} else {
			echo htmlentities($val);
		}
	}
	
	/**
	 * Does not use htmlentities()
	 */
	public function write_raw($key, $def = '') {
		if ($this->has($key)) {
			$val = $this->alist[$key];
		} else {
			$val = $def;
		}
		
		echo $val;
	}
	
	public function set($key, $value, $clear_all = false) {
		if ($clear_all) {
			$this->clear();
		}
		
		$this->alist[$key] = $value;
	}
	
	public function del($key) {
		unset($this->alist[$key]);
	}
	
	public function append($key, $value) {
		if ($this->has($key)) {
			$this->alist[$key] .= $value;
		} else {
			$this->alist[$key] = $value;
		}
	}
	
	public function prepend($key, $value) {
		if ($this->has($key)) {
			$this->alist[$key] = $value . $this->alist[$key];
		} else {
			$this->alist[$key] = $value;
		}
	}
	
	/**
	 * Clears the array (it will delete everything)
	 */
	public function clear() {
		$this->alist = array();
	}
	
	public function size() {
		return count($this->alist);
	}
	
	public function __toString() {
		return print_r($this->alist, true);
	}
}
