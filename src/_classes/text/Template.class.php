<?php
namespace text;

	class Template {
		private $template_file;
		private $template_content;
		private $template_result;
		private $default_attribs;
		
		public function __construct() {
			$this->template_file = "";
			$this->template_content = "";
			$this->template_result = "";
			$this->default_attribs = array();
		}
		
		public function setTemplate($tmpl) {
		    $this->template_content = $tmpl;
		}
		
		public function loadTemplate($fn) {
			$this->template_file = $fn;
			$this->reloadTemplate();
		}
		
		public function reloadTemplate() {
			$this->template_content = file_get_contents($this->template_file);
		}
		
		private function prepareDefaultAttribs() {
			$this->default_attribs = array();
			$this->default_attribs['diahora'] = DateTimeUtils::getNow();
			$this->default_attribs['IP'] = $_SERVER['REMOTE_ADDR'];
			$this->default_attribs['server_name'] = $_SERVER['SERVER_NAME'];
		}
		
		public function apply($attribs) {
			$this->prepareDefaultAttribs();
			$attribs = array_merge($this->default_attribs, $attribs);
			
			$this->template_result = $this->template_content;
			
			$keys = array_keys($attribs);
			
			foreach($keys as $k) {
				$regexp = '/\${' . $k . '}/i';
				$this->template_result = preg_replace($regexp, $attribs[$k], $this->template_result);
			}
			
			//echo $this->template_result;
			
			return $this->template_result;
		}
		
		public function getTemplateResult() {
			return $this->template_result;
		}
		
		public function clearTemplateResult() {
			$this->template_result = "";
		}
	}
?>
