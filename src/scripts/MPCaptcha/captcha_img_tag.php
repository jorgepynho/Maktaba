<?php

	// this file is called like <img src="captcha_img_tag.php" ... >

	session_start();
	
	header('Content-Type: image/jpeg');
	
	$md5 = md5(rand(0,999)); 
	
	//echo $md5 . '<br>' . PHP_EOL;
	
	// substituir os o, O  e 0 para n�o se confundirem entre si...
	$md5 = str_replace('0', rand(1,3), $md5);
	$md5 = str_replace('o', rand(4,6), $md5);
	$md5 = str_replace('O', rand(7,9), $md5);
	
	//echo $md5 . '<br>' . PHP_EOL;
	
	$code = strtolower(substr($md5, 10, 5));
	
	//echo $code . '<br>' . PHP_EOL;
	
	$_SESSION['csc'] = $code;
	
	$width = 150;
    $height = 50;  
	
	$image = imagecreate($width, $height); 
	
	$font = './trebucbd.ttf';
	
    $white = imagecolorallocate($image, 255, 255, 255);
    $black = imagecolorallocate($image, 0, 0, 0);
    $grey = imagecolorallocate($image, 204, 204, 204); 
	$blue = imagecolorallocate($image, 0, 0, 255); 
	$red = imagecolorallocate($image, 255, 0, 0); 
	
    imagerectangle($image, 0, 0, $width-1, $height-1, $black); 
	
	for($n = 0; $n < 6; ++$n) {
		
		$x1 = rand($n * 25, ($n + 1) * 25);
		$y1 = rand(0, 10);
		
		$x2 = rand($n * 25, ($n + 1) * 25);
		$y2 = rand($height - 10, $height);
		
		imageline($image, $x1, $y1, $x2, $y2, $blue); 
		
		$x1 = rand($n * 25, ($n + 1) * 25);
		$y1 = rand(0, 10);
		
		$x2 = rand($n * 25, ($n + 1) * 25);
		$y2 = rand($height - 10, $height);
		
		imageline($image, $x1, $y1, $x2, $y2, $blue); 
	}
	
	$code2 = str_split($code);
	for($n = 0; $n < count($code2); ++$n) {
		
		$char = $code2[$n];
		$size = rand(20, 30);
		
		$angle = rand(-25, 25);
		$x = ($n * 30) + 8;
		$y = 35;
		
		imagettftext($image, $size, $angle, $x, $y, $blue, $font, $char);
	}
	
	imagejpeg($image);
	
	imagedestroy($image); 
?>
