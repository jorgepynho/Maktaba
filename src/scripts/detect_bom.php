<?php

/**
 * Place this file where you want to check the files
 * This script will go to sub-dirs also
 * Better to use this in the CLI
 */

define('TO_DOC_ROOT', '..');
define('RUN_PROFILE_NAME', 'cli');
define('RUN_PROFILE_GROUP', 'cli');

require('../_start.php');

use utils\Utils;

$ignore_list[] = '.';
$ignore_list[] = '..';

function listAndSearch($path, $lv = 0) {
    global $ignore_list;
    
    $files = scandir($path);
    foreach($files as $f) {
	
	if (in_array($f, $ignore_list)) {
	    continue;
	}
	
	$this_path = $path . '/' . $f;
	
	//echo $this_path . PHP_EOL;
	
	if (Utils::fileHasBOM($this_path)) {
	    echo $this_path . PHP_EOL;
	}
	
	if (is_dir($this_path)) {
	    listAndSearch($this_path, ++$lv);
	}
    }
}

listAndSearch('..');
