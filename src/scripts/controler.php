<?php
	while($controler->action_file) {
		
		$debug = false;
		
		if (!$controler->run_actions) {
			if ($debug) echo "<!-- no more actions (1) -->" . PHP_EOL;
			break;
		}
		
		$controler->setDefActionsFiles();
		
		$use_action_file = $controler->action_file;
		
		$controler->action_file = '';
		
		foreach($controler->run_def_actions as &$d) {
			
			if (!$controler->run_actions) {
				if ($debug) echo "<!-- no more actions (2) -->" . PHP_EOL;
				break;
			}
			
			if (!$d->executed) {
				if (is_file($d->path)) {
					
					//var_dump($d);
					
					if ($debug) echo "<!-- " . $d->path . " -->" . PHP_EOL;
					require $d->path;
					$d->executed = true;
				} else {
					if (RUN_PROFILE_GROUP == 'dev') {
						//echo $d->path . " NOT A FILE !";
					}
				}
			}
		}
		
		if (!$controler->run_actions) {
			if ($debug) echo "<!-- no more actions (3) -->" . PHP_EOL;
			break;
		}
		
		if ($use_action_file) {
			if (is_file($use_action_file)) {
				if ($debug) echo "<!-- $use_action_file -->" . PHP_EOL;
				require $use_action_file;
			} else {
				if (RUN_PROFILE_GROUP == 'dev') {
					//echo $use_action_file . " NOT A FILE !";
				}
			}
		}
	}
?>
